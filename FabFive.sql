DROP DATABASE bowlingballblog;
DROP DATABASE bowlingballblog_test;

CREATE DATABASE bowlingballblog;
USE bowlingballblog ;


CREATE TABLE IF NOT EXISTS Category (
  categoryId INT NOT NULL,
  categoryName VARCHAR(45) NOT NULL,
  PRIMARY KEY (categoryId));


CREATE TABLE IF NOT EXISTS Post (
  postId INT NOT NULL AUTO_INCREMENT,
  author VARCHAR(45) NOT NULL,
  postDate DATE NOT NULL,
  content LONGTEXT NOT NULL,
  rtating INT NULL,
  price VARCHAR(15) NOT NULL,
  size INT NOT NULL,
  color VARCHAR(45) NOT NULL,
  weight FLOAT NOT NULL,
  category_categoryId INT NOT NULL,
  PRIMARY KEY (postId),
  CONSTRAINT fk_post_category1
    FOREIGN KEY (category_categoryId)
    REFERENCES category (categoryId));


CREATE TABLE IF NOT EXISTS Hashtag (
  hashtagId INT NOT NULL AUTO_INCREMENT,
  hashtag VARCHAR(45) NULL,
  PRIMARY KEY (hashtagId));


CREATE TABLE IF NOT EXISTS Post_has_Hashtag (
  post_postId INT NOT NULL,
  hashtag_hashtagid INT NOT NULL,
  PRIMARY KEY (post_postId, hashtag_hashtagId),
  CONSTRAINT fk_post_has_hashtag_post
    FOREIGN KEY (post_postId)
    REFERENCES post (postId),
  CONSTRAINT fk_post_has_pashtag_hashtag1
    FOREIGN KEY (hashtag_hashtagId)
    REFERENCES hashtag (hashtagId));



CREATE TABLE IF NOT EXISTS StaticContent (
  staticContentId INT NOT NULL AUTO_INCREMENT,
  staticContent VARCHAR(300) NOT NULL,
  PRIMARY KEY (staticContentId));
  
CREATE DATABASE bowlingballblog_test;
USE bowlingballblog_test ;


CREATE TABLE IF NOT EXISTS Category (
  categoryId INT NOT NULL,
  categoryName VARCHAR(45) NOT NULL,
  PRIMARY KEY (categoryId));


CREATE TABLE IF NOT EXISTS Post (
  postId INT NOT NULL AUTO_INCREMENT,
  author VARCHAR(45) NOT NULL,
  postDate DATE NOT NULL,
  content LONGTEXT NOT NULL,
  rtating INT NULL,
  price VARCHAR(15) NOT NULL,
  size INT NOT NULL,
  color VARCHAR(45) NOT NULL,
  weight FLOAT NOT NULL,
  category_categoryId INT NOT NULL,
  PRIMARY KEY (postId),
  CONSTRAINT fk_post_category1
    FOREIGN KEY (category_categoryId)
    REFERENCES category (categoryId));


CREATE TABLE IF NOT EXISTS Hashtag (
  hashtagId INT NOT NULL AUTO_INCREMENT,
  hashtag VARCHAR(45) NULL,
  PRIMARY KEY (hashtagId));


CREATE TABLE IF NOT EXISTS Post_has_Hashtag (
  post_postId INT NOT NULL,
  hashtag_hashtagid INT NOT NULL,
  PRIMARY KEY (post_postId, hashtag_hashtagId),
  CONSTRAINT fk_post_has_hashtag_post
    FOREIGN KEY (post_postId)
    REFERENCES post (postId),
  CONSTRAINT fk_post_has_pashtag_hashtag1
    FOREIGN KEY (hashtag_hashtagId)
    REFERENCES hashtag (hashtagId));



CREATE TABLE IF NOT EXISTS StaticContent (
  staticContentId INT NOT NULL AUTO_INCREMENT,
  staticContent VARCHAR(300) NOT NULL,
  PRIMARY KEY (staticContentId));